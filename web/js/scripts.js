$(window).load(function() {
    if($().flexslider){

        function initSlider(slider){
            slider.flexslider({
                pauseOnHover: true, //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
                controlsContainer: ".flex-container" //Selector: Declare which container the navigation elements should be appended too. Default container is the flexSlider element. Example use would be ".flexslider-container", "#container", etc. If the given element is not found, the default action will be taken.
            });
            var next = slider.parent().find('.flex-direction-nav .next');
            var prev = slider.parent().find('.flex-direction-nav .prev');

            // Swipe gestures support
            if(Modernizr.touch && $().swipe) {
                function doSliderSwipe(e, dir ) {
                    if(dir.toLowerCase() == 'left' ){
                        next.trigger('click');
                    }
                    if(dir.toLowerCase() == 'right' ){
                        prev.trigger('click');
                    }
                }

                slider.swipe({
                    click       : function(e, target){
                        $(target).trigger('click');
                    },
                    swipeLeft       : doSliderSwipe,
                    swipeRight      : doSliderSwipe,
                    allowPageScroll : 'auto'
                });

            }
        }

        var sliders = $('.flexslider');
        if(sliders.length > 0){
            sliders.each(function() {
                initSlider($(this));
            });
        }

    }

    $(".nav").tinyNav({
        active: 'active', // Set the "active" class
        header: false // Show header instead of the active item
    });

    $('portfolio-list').filterable();

    $('a.dropdown-toggle, .dropdown-menu a').on('touchstart', function(e) {
        e.stopPropagation();
    });
});
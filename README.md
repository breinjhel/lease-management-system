Lease Management System
========================


Installation
------------------

- Make sure the PATH environment variable is set for xampp/php

- Should have the intl extension for php enabled.
	- Check if extension path in  is pointing in "C:\xampp\php\ext"
	- Uncomment these lines in php.ini:
		- extension=php_openssl.dll
		- extension=php_intl.dll
	- Copy the icu*.dll files in apache/bin folder
	- Restart apache
	- Check if intl extension is now installed in phpinfo
	
- Make sure you have a composer.phar file

- Install the dependencies using terminal command

Here's the code:

    php composer.phar install

	
- Optional: Configure xampp vhost and hosts file for better url
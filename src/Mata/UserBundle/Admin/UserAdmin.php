<?php


namespace Mata\UserBundle\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Mata\MataMainBundle\Entity\Product;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mata\AdminBundle\Admin\Admin;

class UserAdmin extends Admin
{
    protected function configureFields()
    {
        return array('username','password','email','isActive');
    }
}
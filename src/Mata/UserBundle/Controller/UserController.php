<?php

namespace Mata\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

use Mata\UserBundle\Entity\User;

class UserController extends Controller
{
	
	public function loginAction()
	{
        // Checks if already logged-in
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            // redirect authenticated users to homepage
            return $this->redirect($this->generateUrl('mata_page_home'));
        }

        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

		return $this->render('MataUserBundle:User:login.html.twig',  array(
			'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
	}
	
	public function registerAction()
	{
		$request = Request::createFromGlobals();
		if($request->getMethod() == 'POST')
		{
			$user = new User();

			$user->setUsername($request->request->get('username'));
			$user->setEmail($request->request->get('email'));
			
			$factory = $this->container->get('security.encoder_factory');
			$encoder = $factory->getEncoder($user);
			$user->setPassword($encoder->encodePassword($request->request->get('password'), $user->getSalt()));
			
			$em = $this->getDoctrine()->getManager();

			$em->persist($user); // Manage the object

			$em->flush();	// Executes the query to persist the managed objects
		}
		
		
		return $this->render('MataUserBundle:User:register.html.twig');
	}
}

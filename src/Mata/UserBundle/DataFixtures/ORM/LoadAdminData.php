<?php

namespace Mata\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mata\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAdminData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user->setUsername("admin");
        $user->setEmail("admin@site.com");
        $user->setRole("ROLE_ADMIN");

        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        $user->setPassword($encoder->encodePassword("admin", $user->getSalt()));

        $manager->persist($user); // Manage the object

        $manager->flush();	// Executes the query to persist the managed objects
    }

}

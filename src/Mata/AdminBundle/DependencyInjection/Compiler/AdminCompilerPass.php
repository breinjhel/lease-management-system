<?php

namespace Mata\AdminBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class AdminCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $admins = array();

        $spool = $container->getDefinition('mata.admin.spool');

        $taggedServices = $container->findTaggedServiceIds('mata.admin');

        foreach ($taggedServices as $id => $attributes) {
            $definition = $container->getDefinition($id);
            $arguments = $definition->getArguments();

            $admins[] = $id;
        }

        $spool->addMethodCall('setAdmins', array($admins));
    }
}

<?php

namespace Mata\AdminBundle\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

interface AdminInterface
{
    public function setEntity(Entity $entity);
}

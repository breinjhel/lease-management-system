<?php

namespace Mata\AdminBundle\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

// Responsible for Managing the Admins
// Getting info such as EntityNamespace, Fieldnames and Rows

class AdminManager extends  ContainerAware
{
    protected $serviceId;

    protected $entityNamespace = null;

    protected $fieldNames = array();

    protected $fields;

    protected $rows;


    public function __construct($container,$serviceId)
    {
        $this->container = $container;

        // Gets all admin service Ids
        $adminsId = $this->container->get('mata.admin.spool')->getAdmins();

        // Checks what admin is currently active based on route parameter
        foreach($adminsId as $key => $value)
        {
            // Sets the Entity of the current active Admin
            if($value == $serviceId){
                $this->entityNamespace = $this->container->get($value)->getEntity();
                $this->fieldNames = $this->container->get($value)->getFields();
            }
        }

        // Checks and throws exception if Entity is null
        if($this->entityNamespace == null)
        {
            throw new NotFoundHttpException("Page not found");
        }
    }


	public function getAllFieldNames(){
	
        $em = $this->getDoctrine()->getManager();
	    
		// Gets the metadata of the entity to get all fieldnames
        $MetaData = $em->getClassMetadata($this->entityNamespace);

        // Sets the all fieldnames since nothing is specified
        return $MetaData->fieldNames;
	}
	
    public function getFieldNames()
    {
        $em = $this->getDoctrine()->getManager();

        // Checks if Field Names array to be visible is empty
        if(!$this->fieldNames){
            // Gets the metadata of the entity to get all fieldnames
            $MetaData = $em->getClassMetadata($this->entityNamespace);

            // Sets the all fieldnames since nothing is specified
            
            $fields = $MetaData->fieldNames;
        
            unset($fields['id']);
            return $fields;
        }

        return $this->fieldNames;
    }

    public function setFieldNames($fieldNames)
    {
        $this->fieldNames = $fieldNames;
    }


    public function getRows()
    {
        // Gets Entity Manager object
        $em = $this->getDoctrine()->getManager();
		
		//$numItems = count($this->fieldNames);
		//$i = 0;
        //if($this->fieldNames){
        //    $fieldarr = implode(', ', $this->fieldNames);
        //   $dql = 'SELECT ';
		//	foreach($this->fieldNames as $field){
		//		if(++$i === $numItems){
		//			$dql .= 'u.'.$field.' ';
		//			break;
		//		}
		//		$dql .= 'u.'.$field.', ';
		//	}
        //    //$dql .= $fieldarr;
        //    $dql .= ' ';
        //   $dql .= 'FROM '.$this->entityNamespace.' u';
        //}else{
            $dql = 'SELECT a FROM '.$this->entityNamespace.' a';

            // Gets the metadata of the entity to get all fieldnames
            $MetaData = $em->getClassMetadata($this->entityNamespace);

            // Sets the all fieldnames since nothing is specified
            $this->fieldNames = $MetaData->fieldNames;
        //}

        $query = $em->createQuery($dql);
        $rows = $query->getArrayResult();

        return $rows;
    }

    public function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    public function getEntityNamespace()
    {
        return $this->entityNamespace;
    }
	
	public function deleteRow($id)
	{
		// Gets Entity Manager object
        $em = $this->getDoctrine()->getManager();
		
		$dql = 'DELETE FROM '.$this->entityNamespace.' u where u.id = '.$id;
		
		
        $query = $em->createQuery($dql);
		
		$results = $query->getResult();
		
		return true;
	}
	
	public function updateRow($id,$newFields)
	{
		// Gets Entity Manager object
        $em = $this->getDoctrine()->getManager();
		
		$dql = 'UPDATE '.$this->entityNamespace.' u SET ';
		
		$numItems = count($this->fieldNames);
		$i = 0;
		foreach ($newFields as $key => $value) {
			foreach ($this->fieldNames as $field) {
				if($key == $field){
					if(++$i === $numItems){
						$dql .= 'u.'.$key.'='.$value.' ';
						break;
					}
					$dql .= 'u.'.$key.'='.$value.', ';
				}
			}
		}
		
		$dql .= 'where u.id = '.$id;
		
		
        $query = $em->createQuery($dql);
		
		$results = $query->getResult();
		
		return true;
	}
	
	public function getEntity($id) 
	{
		// Gets Entity Manager object
		$em = $this->getDoctrine()->getManager();
		
		return $em->getRepository($this->entityNamespace)->find($id);
	}
}

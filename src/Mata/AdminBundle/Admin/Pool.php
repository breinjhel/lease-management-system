<?php

namespace Mata\AdminBundle\Admin;

use Mata\AdminBundle\Admin\AdminManager;


class Pool
{
    protected $container;
	
    protected $admins = array();
	
    protected $entity;


    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFields()
    {
    }

    public function addAdmin($admin)
    {
        $this->admins[] = $admin;
    }

    public function getAdmins()
    {
        return $this->admins;
    }

    public function setAdmins($admins)
    {
        $this->admins = $admins;
    }

    public function generateNav()
    {

    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }
	
	public function getContainer()
	{
		return $this->container;
	}

    public function getAdminManager($serviceId)
    {
        return new AdminManager($this->container,$serviceId);
    }
}

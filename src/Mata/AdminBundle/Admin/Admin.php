<?php

namespace Mata\AdminBundle\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Mata\AdminBundle\Admin\AdminInterface;

abstract class Admin implements AdminInterface
{
    protected $entity;
	protected $fields = array();

    public function __construct($entity)
    {
        $this->entity = $entity;
		$this->fields = $this->configureFields();
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

	public function getFields()
	{
		return $this->fields;
	}
	
    abstract protected function configureFields();
}

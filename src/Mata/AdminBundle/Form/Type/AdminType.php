<?php

namespace Mata\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AdminType extends AbstractType
{
    protected $dataClass;

    protected $fieldNames;

    public function __construct($dataClass, $fieldNames)
    {
        $this->dataClass = $dataClass;
        $this->fieldNames = $fieldNames;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach($this->fieldNames as $fieldName){
            $builder->add($fieldName);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->dataClass,
        ));
    }

    public function getName()
    {
        return 'admin';
    }
}

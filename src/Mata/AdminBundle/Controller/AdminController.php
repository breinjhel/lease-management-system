<?php

namespace Mata\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Mata\AdminBundle\Admin\AdminManager;
use Mata\AdminBundle\Form\Type\AdminType;

class AdminController extends Controller
{	
    protected $adminsId = array();
 
	protected $entity = null;
	
	protected $fieldNames = array();
	
	protected $entityObj = null;
	
	protected $am;
	
    public function indexAction()
    {
        return $this->render('MataAdminBundle:Admin:index.html.twig');
    }


    public function listAction($adminName)
    {
        //TODO: Generate the nav by getting all admin classes


		$this->am = $this->get('mata.admin.spool')->getAdminManager('mata.admin.'.$adminName);
		
		$this->fieldNames = $this->am->getFieldNames();
		
		$allFieldNames = $this->am->getAllFieldNames();

		$rows = $this->am->getRows();

        return $this->render('MataAdminBundle:Admin:list.html.twig',array(
			'title' =>  ucfirst($adminName), 
			'rows' => $rows, 
			'fields' => $this->fieldNames,
			'allFields' => $allFieldNames
		));
    }

    public function addAction(Request $request,$adminName)
    {
        $this->am = $this->get('mata.admin.spool')->getAdminManager('mata.admin.'.$adminName);

        $this->entity = $this->am->getEntityNamespace();
        $this->fieldNames = $this->am->getFieldNames();

        $entity = new $this->entity;

        $form = $this->createForm(new AdminType($this->entity, $this->fieldNames), $entity);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
				
				$this->get('session')->getFlashBag()->add('notice', 'Successfully added new '.$adminName.'.');
				
                return $this->redirect($this->generateUrl('mata_admin_add', array('adminName'=>$adminName)));
            }
        }

        return $this->render('MataAdminBundle:Admin:add.html.twig',array(
            'title' =>  ucfirst($adminName),
            'fields' => $this->fieldNames,
            'form' => $form->createView()
            )
        );
    }
	
	public function deleteAction($adminName, $id)
	{
		$this->am = $this->get('mata.admin.spool')->getAdminManager('mata.admin.'.$adminName);

		$this->am->deleteRow($id);
		
		return $this->redirect($this->generateUrl('mata_admin_list', array('adminName'=>$adminName)));
	}	
	
	public function updateAction(Request $request,$adminName, $id) 
	{
        $this->am = $this->get('mata.admin.spool')->getAdminManager('mata.admin.'.$adminName);

        $this->entity = $this->am->getEntityNamespace();
        $this->fieldNames = $this->am->getFieldNames();

        $entity = $this->am->getEntity($id);

        $form = $this->createForm(new AdminType($this->entity, $this->fieldNames), $entity);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
				
				$this->get('session')->getFlashBag()->add('notice', 'Successfully updated '.$adminName.'.');
				
                return $this->redirect($this->generateUrl('mata_admin_update', array(
                	'adminName'=>$adminName,
                	'id' => $id
                	)));
            }
        }

        return $this->render('MataAdminBundle:Admin:edit.html.twig',array(
            'title' =>  ucfirst($adminName),
            'fields' => $this->fieldNames,
            'form' => $form->createView(),
            'id' => $id
            )
        );
	}
}

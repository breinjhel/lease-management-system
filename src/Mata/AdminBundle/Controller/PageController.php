<?php

namespace Mata\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function indexAction($name)
    {
		$repository = $this->getDoctrine()->getRepository('MataAdminBundle:Page');
		
		$page = $repository->getPage($name);
		
        return $this->render('MataAdminBundle:Page:index.html.twig', array('content' => $page->getContent()));
    }
}

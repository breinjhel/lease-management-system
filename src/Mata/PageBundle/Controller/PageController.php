<?php

namespace Mata\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('MataPageBundle:Page:index.html.twig');
    }

    public function homeAction()
    {
        return $this->render('MataPageBundle:Page:home.html.twig');
    }

    public function contactAction(Request $request)
    {
        if($request->getMethod() == 'POST'){
            $subject = $request->request->get('subject');
            $sender_name = $request->request->get('name');
            $sender_email = $request->request->get('email');
            $content = $request->request->get('content');

            $message = \Swift_Message::newInstance()
                ->setSubject("Customer contact")
                ->setFrom($sender_email)
                ->setTo('breinjhel@gmail.com')
                ->setBody("From: ". $sender_name . "\nEmail: ". $sender_email . "\nSubject: ". $subject . "\nContent: \n" . $content);
            ;
            $this->get('mailer')->send($message);

            $this->get('session')->getFlashBag()->add('notice', 'Message sent!');
        }



        return $this->render('MataPageBundle:Page:contact.html.twig');
    }
}

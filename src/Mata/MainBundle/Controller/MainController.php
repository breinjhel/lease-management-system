<?php


namespace Mata\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Mata\MataMainBundle\Entity\Product;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class MainController extends Controller
{
    public function indexAction()
    {
	
        return $this->render('MataMainBundle:Default:index.html.twig');
    }
	
	
	public function showAction($id)
	{
		
		return $this->render('MataMainBundle:Default:property.html.twig');
	}


//    public function searchAction(Request $request)
//    {
//        $product = new Product();
//
//        $request = Request::createFromGlobals();
//
//        // $_GET parameters
//        $request->query->get('q');
//
//        return $this->render('MataMainBundle:Page:index.html.twig', array('pageTitle' =>  'Search'));
//    }
//
//
//    public function postAction()
//    {
//        $request = Request::createFromGlobals();
//
//
//        if($request->getMethod() == 'POST')
//        {
//            $product = new Product();
//
//            $product->setName($request->request->get('name'));
//            $product->setDescription($request->request->get('description'));
//            $product->setPrice($request->request->get('price'));
//
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($product); // Manage the object
//
//            $em->flush();	// Executes the query to persist the managed objects
//        }
//
//        return $this->render('MataMainBundle:Page:post.html.twig', array('pageTitle' =>  'Post a free Ad'));
//    }
//
//    public function rentAction($id)
//    {
//
//        //if($id){
//        //$repository = $this->getDoctrine()->getRepository('MataClassifiedAdsBundle:Room');
//
//        //$room = $repository->findOneById($id);
//        //return $this->render('MataClassifiedAdsBundle:Page:rent.html.twig', array('pageTitle' =>  'Rent', 'room' => $room));
//        //}
//        return $this->render('MataMainBundle:Page:rent.html.twig', array('pageTitle' =>  'Rent'));
//    }
}
